// SevenAround - Constructing Models of the "Seven Around" Surface
// Copyright (c) 2009-2013 Mark Howison
// 
// This file is part of SevenAround.
// 
// SevenAround is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// SevenAround is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with SevenAround.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SEVENAROUND_HALFEDGE_H
#define	__SEVENAROUND_HALFEDGE_H

#include <Tiny3D/Tiny3D.h>

using namespace Tiny3D;

class Vertex;
class Face;

class HalfEdge
{
	public:

	HalfEdge(Vertex* _origin);
	void pair(HalfEdge* e);
	void append(HalfEdge* e);
	HalfEdge* findLeadingEdge();
	HalfEdge* findTrailingEdge();

	Vertex* origin;
	HalfEdge* next;
	HalfEdge* prev;
	HalfEdge* sibling;
	HalfEdge* pop;
	int id;
};

class Vertex : public Vector3
{
	public:

	Vertex(double x, double y, double z, int _level, int _degree);
	void dump();

	HalfEdge* leadedge;
	int level;
	int degree;
	int id;
	Vertex* pop;
};

class Face : public Vector3
{
	public:

	Face(HalfEdge* _e1, HalfEdge* _e2, HalfEdge* _e3, int _level);
	bool isAdjacent(Face* f) const;
	void setCenter(double _x, double _y, double _z);

	HalfEdge* e1;
	HalfEdge* e2;
	HalfEdge* e3;
	int level;
	Face* pop;
};

#endif

