// SevenAround - Constructing Models of the "Seven Around" Surface
// Copyright (c) 2009-2013 Mark Howison
// 
// This file is part of SevenAround.
// 
// SevenAround is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// SevenAround is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with SevenAround.  If not, see <http://www.gnu.org/licenses/>.

#include "HalfEdge.h"

using namespace std;

HalfEdge::HalfEdge(Vertex* _origin)
{
	origin = _origin;
	origin->leadedge = this;
	next = NULL;
	prev = NULL;
	sibling = NULL;
	id = -1;
}

void HalfEdge::pair(HalfEdge* e)
{
	sibling = e;
	e->sibling = this;
}

void HalfEdge::append(HalfEdge* e)
{
	next = e;
	e->prev = this;
}

/* the leading edge is the one furthest clockwise
 * around the vertex */
HalfEdge* HalfEdge::findLeadingEdge()
{
	HalfEdge* e = this;
	while (true) {
		if (e->sibling == NULL) return e;
		e = e->sibling->next;
		if (e == this) return NULL;
	}
}

/* the trailing edge is the one furthest counter-clockwise
 * around the vertex */
HalfEdge* HalfEdge::findTrailingEdge()
{
	assert(this->prev != NULL);

	HalfEdge* e = this->prev;
	while (true) {
		if (e->sibling == NULL) return e;
		e = e->sibling->prev;
		if (e == this->prev) return NULL;
	}
}


Vertex::Vertex(double _x, double _y, double _z, int _level, int _degree)
{
	x = _x;
	y = _y;
	z = _z;
	level = _level;
	degree = _degree;
}

void Vertex::dump()
{
	cout << x << " " << y << " " << z << " " << level << " " << degree << endl;
}


Face::Face(HalfEdge* _e1, HalfEdge* _e2, HalfEdge* _e3, int _level)
{
	e1 = _e1;
	e2 = _e2;
	e3 = _e3;
	level = _level;

	/* set center */
	Vector3 center;
	center = *(e1->origin)
		   + (2.0/3.0) * ( *(e2->origin) - *(e1->origin)
						   + 0.5*( *(e3->origin) - *(e2->origin) ) );

	x = center.x;
	y = center.y;
	z = center.z;
}

bool Face::isAdjacent(Face* f) const
{
	if (e1->sibling == f->e1) return true;
	if (e1->sibling == f->e2) return true;
	if (e1->sibling == f->e3) return true;
	if (e2->sibling == f->e1) return true;
	if (e2->sibling == f->e2) return true;
	if (e2->sibling == f->e3) return true;
	if (e3->sibling == f->e1) return true;
	if (e3->sibling == f->e2) return true;
	if (e3->sibling == f->e3) return true;
	return false;
}

void Face::setCenter(double _x, double _y, double _z)
{
	x = _x;
	y = _y;
	z = _z;
}

