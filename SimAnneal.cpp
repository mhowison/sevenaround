// SevenAround - Constructing Models of the "Seven Around" Surface
// Copyright (c) 2009-2013 Mark Howison
// 
// This file is part of SevenAround.
// 
// SevenAround is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// SevenAround is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with SevenAround.  If not, see <http://www.gnu.org/licenses/>.

#include <math.h>
#include <vector>
#include "Surface.h"

static int attempts = 1;
static bool verbose = false;
static bool log = false;
static vector<Surface*> models;
static Surface* model;
static Surface* top_model;
static Surface* top_surface;

void Run()
{
	MTRand randgen;
	randgen.seed();
	
	unsigned i = 1;
	while (i++)
	{
		/* simulated annealing */
		Surface* candidate = models.at(randgen.randInt(models.size()));
		if (exp(candidate.getRating() - model->getRating()) >
															randgen.randExc())
		{
			model = candidate;
		}

		surface = new Surface(model);
		surface->newLevel();

		if (surface->completeLevel(attempts)) {
			/* Complete surfaces become new models. */
			surface->makeModel(models.size(), model->getID());
			models.append(surface);
			model->updateRating(1);
			if (model->getRating() > top_model->getRating())
				top_model = model;
		} else {
			/* Incomplete surfaces may be in the top ten. */
			if (surface->size() > top_surface->size()) {
				delete(top_surface);
				top_surface = surface;
				top_surface->setParent(model->getID());
			}
			model->updateRating(0);
		}
	}
}

void loadModels(char* infile)
{
	hid_t h5file = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
	model = new Surface(h5file, id);
	models.append(model);
	H5Fclose(h5file);
}

void PrintUsage()
{
	cout << "\n"
"usage: SevenAround [-i INFILE] [-m MODEL] [-o OUTFILE]\n"
"                   [-a ATTEMPTS] [-b BOUNDARY] [-lv]\n"
"\n"
"  -i  read a model from the input HDF5 file INPUT\n"
"  -m  specifiy the HDF5 path to MODEL in INPUT (default: '/')\n"
"  -o  write HDF5 output to OUTFILE\n"
"  -a  maximum number of ATTEMPTS to complete the next level\n"
"  -b  build boundary only out to BOUNDARY levels\n"
"  -l  use a logarithmic curve for the boundary (not used if MODEL is specified)\n"
"  -v  verbose output\n"
"\n";
	exit(EXIT_FAILURE);
}

int main(int argc, char** argv)
{
	char* infile = NULL;
	char* outfile = NULL;

	int c;
	while ((c = getopt(argc, argv, "i:o:a:vlsh")) != -1)
		switch (c) {
			case 'i':
				infile = optarg;
				break;
			case 'o':
				outfile = optarg;
				break;
			case 'a':
				attempts = atoi(optarg);
				break;
			case 'v':
				verbose = true;
				break;
			case 'l':
				log = true;
				break;
			case 's':
				chooser = seed;
				break;
			case 'h':
			default:
				PrintUsage();
		}

	if (infile == NULL) {
		model = top_model = top_surface = new Surface(log);
		model->makeModel(0, -1);
		models.append(model);
	} else {
		LoadModels(infile);
	}

	hid_t file = H5Fcreate(filename,H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);

	/* Install signal handler for kill. */

	Run();
}
