// SevenAround - Constructing Models of the "Seven Around" Surface
// Copyright (c) 2009-2013 Mark Howison
// 
// This file is part of SevenAround.
// 
// SevenAround is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// SevenAround is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with SevenAround.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <hdf5.h>
#include "Surface.h"

void print_usage()
{
	cout << "\n"
"usage: SevenAround [-i INFILE] [-m MODEL] [-o OUTFILE]\n"
"                   [-a ATTEMPTS] [-b BOUNDARY] [-lv]\n"
"\n"
"  -i  read a model from the input HDF5 file INPUT\n"
"  -m  specifiy the HDF5 path to MODEL in INPUT (default: '/')\n"
"  -o  write HDF5 output to OUTFILE\n"
"  -a  maximum number of ATTEMPTS to complete the next level\n"
"  -b  build boundary only out to BOUNDARY levels\n"
"  -l  use a logarithmic curve for the boundary (not used if MODEL is specified)\n"
"  -v  verbose output\n"
"\n";
	exit(EXIT_FAILURE);
}

int main (int argc, char** argv)
{
	char* infile = NULL;
	char* model = NULL;
	char* outfile = NULL;
	int attempts = 1;
	int boundary = 0;
	bool verbose = false;
	bool log = false;

	int c;
	while ((c = getopt(argc, argv, "i:m:o:a:b:lvh")) != -1)
		switch (c) {
			case 'i':
				infile = optarg;
				break;
			case 'm':
				model = optarg;
				break;
			case 'o':
				outfile = optarg;
				break;
			case 'a':
				attempts = atoi(optarg);
				break;
			case 'b':
				boundary = atoi(optarg);
				break;
			case 'v':
				verbose = true;
				break;
			case 'l':
				log = true;
				break;
			case 'h':
			default:
				print_usage();
		}

	Surface* surface;
	if (infile) {
		hid_t file = H5Fopen(infile, H5F_ACC_RDONLY, H5P_DEFAULT);
		surface = new Surface(file, model);
	} else {
		surface = new Surface(log);
	}

	bool success = true;
	if (boundary) {
		for (int i=2; i<boundary; i++) surface->newLevel();
	} else {
		surface->newLevel();
		success = surface->completeLevel(attempts);
	}

	if (verbose) {
		surface->dump();
		surface->dumpIncomplete();
	}
	
	if (outfile) {
		hid_t file = H5Fcreate(outfile,H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);
		surface->writeHDF5(file);
	}

	return(!success);
}
