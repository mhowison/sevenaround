# SevenAround - Constructing Models of the "Seven Around" Surface
# Copyright (c) 2009-2013 Mark Howison
# 
# This file is part of SevenAround.
# 
# SevenAround is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SevenAround is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SevenAround.  If not, see <http://www.gnu.org/licenses/>.

import os
import sqlite3

def connect(filename):
	global connection
	global cursor
	if not os.path.isfile(filename):
		connection = sqlite3.connect(filename)
		#connection.execute("PRAGMA synchronous=OFF")
		connection.execute("""CREATE TABLE models (
				id INTEGER PRIMARY KEY AUTOINCREMENT,
				level INTEGER,
				parent INTEGER,
				attempts INTEGER,
				successful INTEGER,
				rating REAL )""")
		connection.execute("CREATE INDEX level ON models(level)")
		connection.execute("CREATE INDEX rating ON models(rating)")
		connection.execute("INSERT INTO models VALUES (0,1,-1,0,0,1.0)")
		commit()
	else:
		connection = sqlite3.connect(filename)
	connection.row_factory = sqlite3.Row
	cursor = connection.cursor()

def insert(table, fields, values):
	stmt = "INSERT INTO %s (%s) VALUES (%s)" % (
			table,
			','.join(fields),
			','.join('?' * len(fields)))
	cursor.execute(stmt, values)

def high_model():
	result = cursor.execute("SELECT id, rating FROM models ORDER BY level DESC, rating DESC LIMIT 1").fetchone()
	return (result['id'], result['rating'])

def random_model():
	result = cursor.execute("SELECT id, rating FROM models ORDER BY RANDOM() LIMIT 1").fetchone()
	return (result['id'], result['rating'])

def new_model(surface):
	fields = ('level', 'parent', 'attempts', 'successful', 'rating')
	values = (int(surface['level']), int(surface['parent']), 0, 0, 1.0)
	insert('models', fields, values)
	return cursor.lastrowid

def update_model(id, success):
	result = cursor.execute("SELECT attempts, successful FROM models WHERE id=?", (id,)).fetchone()
	attempts = result['attempts'] + 1
	successful = result['successful'] + success
	rating = float(successful) / float(attempts)
	cursor.execute("UPDATE models SET attempts=?, successful=?, rating=? WHERE id=?", (attempts, successful, rating, id))
	commit()
	return rating

def commit():
	connection.commit()

