// SevenAround - Constructing Models of the "Seven Around" Surface
// Copyright (c) 2009-2013 Mark Howison
// 
// This file is part of SevenAround.
// 
// SevenAround is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// SevenAround is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with SevenAround.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SEVENAROUND_SURFACE_H
#define	__SEVENAROUND_SURFACE_H

#include <vector>
#include <deque>
#include "HalfEdge.h"
#include "MersenneTwister.h"

#define HALF_SQRT2  0.707106781186548 
#define HALF_SQRT3  0.86602540378443864676372317075294
#define FOUR_THIRDS 1.33333333333333333333333333333334

#define EPSILON	 1E-6

#define FACE_BOUNDARY true
#define FACE_NON_BOUNDARY false

#define MAX_LEVEL 16

using namespace std;

class Surface {

public:
	Surface(const bool _logBoundary);
	Surface(const Surface* orig);
	Surface(hid_t file, const char* model);
	virtual ~Surface();
	bool newLevel();
	bool completeLevel(int maxAttempts);
	void writeOOGL(ostream& out) const;
	void writeHDF5(hid_t file) const;
	void dump() const;
	void dumpIncomplete();
	void makeModel(int id, int parent);
	void updateRating(int success);
	int size();
	float getRating();
	int getID();
	void setParent(int parent);
	static MTRand randgen;

private:
	void clear();
	bool fitEquilateralTriangles(
			HalfEdge* E1,
			HalfEdge* E2,
			int direction,
			int level,
			bool isBoundary);
	Vertex* newVertex(double x, double y, double z, int level);
	Vertex* newVertex(Vector3 V, int level);
	Vertex* newVertex(Vector3* V, int level);
	HalfEdge* newHalfEdge(Vertex* origin);
	Face* newFace(Vertex* V1, Vertex* V2, Vertex* V3, int level);
	Face* newFace(HalfEdge* E, Vertex* V, int level);
	Face* newFace(HalfEdge* E1, HalfEdge* E2, int level);
	Face* newFace(HalfEdge* E, double angle, int level);
	void deleteVertex();
	void deleteFace();
	double distSq(Vertex* V1, Vertex* V2) const;
	double deg2rad(double rad) const;
	double orient3dfast(
		   double *pa,
		   double *pb,
		   double *pc,
		   double *pd) const;
	bool detectCollision(Face* f1, Face* f2) const;
	bool validateFace(bool isBoundary) const;

	int currentLevel;

	HalfEdge* xBoundary;
	HalfEdge* yBoundary;
	int xBoundaryType;
	int yBoundaryType;
	bool logBoundary;

	int vcount;
	int ecount;
	int fcount;

	Vertex* vstack;
	HalfEdge* estack;
	Face* fstack;

	deque<Vertex*> incomplete[MAX_LEVEL];

	int id;
	int parent;
	int attempts;
	int successes;
	float rating;
};

#endif

