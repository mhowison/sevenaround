HDF5ROOT=$(HDF5_DIR)

CXX=g++
AR=ar
RANLIB=ranlib

CXXFLAGS=-g -O2 -I$(HDF5ROOT)/include -I.
LDFLAGS=-L$(HDF5ROOT)/lib -L./Tiny3D
LIBS = -ltiny3d -lhdf5_hl -lhdf5 -lz

LIBTINY3D_OBJS = Tiny3D/Matrix3.o Tiny3D/Quaternion.o Tiny3D/Vector3.o
SEVENAROUND_OBJS = SevenAround.o Surface.o HalfEdge.o

.phony: clean

all: libtiny3d SevenAround

libtiny3d:	$(LIBTINY3D_OBJS)
	$(AR) r Tiny3D/libtiny3d.a $(LIBTINY3D_OBJS)
	$(RANLIB) Tiny3D/libtiny3d.a

SevenAround:	$(SEVENAROUND_OBJS)
	$(CXX) $(LDFLAGS) -o SevenAround $(SEVENAROUND_OBJS) $(LIBS)

clean:
	rm -f $(LIBTINY3D_OBJS) $(SEVENAROUND_OBJS) Tiny3D/libtiny3d.a SevenAround

