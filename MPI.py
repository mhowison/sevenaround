# SevenAround - Constructing Models of the "Seven Around" Surface
# Copyright (c) 2009-2013 Mark Howison
# 
# This file is part of SevenAround.
# 
# SevenAround is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SevenAround is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SevenAround.  If not, see <http://www.gnu.org/licenses/>.

import sys
from mpi4py.MPI import *

# Rank of root MPI task.
ROOT = 0

_rank = COMM_WORLD.Get_rank()
_excepthook = sys.excepthook

# Override exception handler to print both the trace and call MPI_Abort().
def excepthook(exctype, value, traceback):
	_excepthook(exctype, value, traceback)
	print >>sys.stderr, "[exception on MPI rank %d]" % _rank
	COMM_WORLD.Abort(1)

sys.excepthook = excepthook

