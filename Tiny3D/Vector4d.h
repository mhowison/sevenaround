#ifndef TINY3D_VECTOR4D_H
#define TINY3D_VECTOR4D_H

typedef double __double4 __attribute__ ((mode(V4DF)));

namespace Tiny3D
{
    class Vector4d
    {

    public:
        /* constructors */
        Vector4d();
        Vector4d(double x, double y, double z, double w);
        Vector4d(const double *v);
        Vector4d(double s);
        Vector4d(const Vector4d& v);

        /* operators */
        double operator[] (const size_t i) const;

        Vector4d& operator= (const Vector4d& v);
        Vector4d& operator= (double s);

        bool operator== (const Vector4d& v) const;
        bool operator!= (const Vector4d& v) const;

        Vector4d operator+ (const Vector4d& v) const;
        Vector4d operator- (const Vector4d& v) const;
        Vector4d operator- () const;
        Vector4d operator* (double s) const;
        Vector4d operator* (const Vector4d& v) const;
        Vector4d operator/ (double s) const;
        Vector4d operator/ (const Vector4d& v) const;

        Vector4d& operator+= (const Vector4d& v);
        Vector4d& operator+= (double s);
        Vector4d& operator-= (const Vector4d& v);
        Vector4d& operator-= (double s);
        Vector4d& operator*= (const Vector4d& v);
        Vector4d& operator*= (double s);
        Vector4d& operator/= (double s);

        /* accessors */
        __double4& get();

        /* mutators */
        void set(__double4 v);
        void set(double x, double y, double z, double w);

        /* methods */
        double length() const;
        double squaredLength() const;
        double distance(const Vector4d& v) const;
        double squaredDistance(const Vector4d& v) const;
        double normalize();
        Vector4d normalizedCopy() const;
        Vector4d abs() const;
        Vector4d cross(const Vector4d& v) const;
        Vector4d mid(const Vector4d& v) const;

        /* special vectors */
        static const Vector4d ZERO;
        static const Vector4d UNIT_X;
        static const Vector4d UNIT_Y;
        static const Vector4d UNIT_Z;
        static const Vector4d UNIT_W;

        /* stream output */
        friend std::ostream& operator<< (std::ostream& o, const Vector4d& v);

    private:
        __double4 vec;
    }
}

#endif

