#ifndef TINY3D_PREREQ_H
#define TINY3D_PREREQ_H

#include <iostream>
#include <cmath>
#include <cassert>

#define PI      3.14159265358979323846
#define TWO_PI  6.28318530717958647692
#define SQRT2   1.4142135623730951
#define SQRT3   1.7320508075688772935274463415059

namespace Tiny3D
{

    /** In order to avoid finger-aches :)
    */
    typedef unsigned char uchar;
    typedef unsigned short ushort;
    typedef unsigned int uint;
    typedef unsigned long ulong;

    typedef double Real;
    typedef double Radian;
    //typedef int size_t;

    // Pre-declare classes
    // Allows use of pointers in header files without including individual .h
    // so decreases dependencies between files
    class Matrix3;
    class Quaternion;
    class Vector3;

}

#endif


