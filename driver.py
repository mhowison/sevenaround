#!/usr/bin/env python
# SevenAround - Constructing Models of the "Seven Around" Surface
# Copyright (c) 2009-2013 Mark Howison
# 
# This file is part of SevenAround.
# 
# SevenAround is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SevenAround is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SevenAround.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import time
import socket
import signal
import subprocess
import argparse
import math
from collections import namedtuple
from operator import attrgetter
from random import random

import h5py
import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

# Paths
DB_PATH = 'models.sqlite'
TOP10_DIR = 'top10'
TOP10_PATH = 'top10.txt'
MODELS_PATH = 'models.h5'
HDF5_EXT = 'h5'

# Message tags.
ASSIGN = 1
COMPLETE = 2

# Keep track of the number of assignments for each worker.
assignments = [0] * comm.Get_size()

def add_model(filename, group_id):
	subprocess.call(['h5copy', '-i', filename, '-o', MODELS_PATH,
										'-s', '/', '-d', '/%d' % group_id])

# top10 is a list of (faces, level, name) tuples
TopTuple = namedtuple('TopTuple', "faces level name")

def load_top10():
	if not os.path.isdir(TOP10_DIR):
		os.mkdir(TOP10_DIR)
	top10 = list()
	if os.path.isfile(TOP10_PATH):
		with open(TOP10_PATH, 'r') as f:
			for line in f:
				fields = line.strip().split('\t')
				top10.append(TopTuple(int(fields[0]), int(fields[1]), fields[2]))
	else:
		print "Top10 file '%s' does not exist... creating empty list." % TOP10_PATH
	return top10

def sort_top10(top10):
	# Sort on the faces field of the tuple.
	return sorted(top10, key=attrgetter('faces'), reverse=True)

def dump_top10(top10):
	with open(TOP10_PATH + '~', 'w') as f:
		for top in top10:
			print >>f, '\t'.join([str(x) for x in top])
	os.rename(TOP10_PATH + '~', TOP10_PATH)

def SevenAround(outfile, model_id, log=False, attempts=1000):
	"""Wrapper for running the SevenAround command and parsing the output."""
	cmd = ['./SevenAround', '-o', outfile, '-a', str(attempts)]
	if model_id:
		cmd += ('-i', MODELS_PATH, '-m', '/%d' % model_id)
	if log:
		cmd.append('-l')

	# Call SevenAround.
	ret = subprocess.call(cmd, stdout=None, stderr=None)

	# Build surface dictionary based on output.
	surface = { 'outfile': outfile, 'parent': model_id }
	if ret == 0:
		surface['complete'] = True
	with h5py.File(outfile, 'r') as f:
		surface['level'] = f.attrs['currentLevel'][0]
		surface['faces'] = len(f['F_level'])
	return surface

def launch_controller(seed=False):

	# Initialize the database and top10 list.
	import database as db
	db.connect(DB_PATH)

	top10 = sort_top10(load_top10())
	# top10 is sorted, so the last entry in top10.txt has the min face count.
	min_top_faces = 0 if len(top10) == 0 else top10[-1].faces

	# Pick the initial model (start with the highest rating)
	model_id, model_rating = db.high_model()

	# Receive messages.
	status = MPI.Status()
	while 1:
		# Block while waiting for messages from workers.
		msg = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
		tag = status.Get_tag()
		worker = status.Get_source()

		if tag == ASSIGN:
			assignments[worker] += 1
			# Send a new work assignment.
			if seed:
				# Generate new low-level models.
				comm.send(None, dest=worker, tag=ASSIGN)
			else:
				# Choose a model with simulated annealing.
				candidate_id, candidate_rating = db.random_model()
				if math.exp(candidate_rating - model_rating) > random():
					model_id = candidate_id
					model_rating = candidate_rating
				comm.send(model_id, dest=worker, tag=ASSIGN)

		elif tag == COMPLETE:

			# Complete surfaces become new models.
			if msg.get('complete'):
				# Reserve a row in the database for the completed model.
				new_id = db.new_model(msg)
				if not os.path.isfile(msg['outfile']):
					print >> stderr, "No output file for completed model!"
					comm.Abort(1)
				add_model(msg['outfile'], new_id)
				db.commit()
				model_rating = db.update_model(model_id, 1)

			# Incomplete surfaces may be in the top ten.
			else:
				if msg['faces'] > min_top_faces:
					name = '.'.join((socket.gethostname(), str(time.time()), HDF5_EXT))
					old = None if len(top10) < 10 else top10.pop()
					top10.append(TopTuple(msg['faces'], msg['level'], name))
					top10 = sort_top10(top10)
					os.rename(msg['outfile'], os.path.join(TOP10_DIR, name))
					dump_top10(top10)
					if old:
						os.remove(os.path.join(TOP10_DIR, old.name))
					min_top_faces = top10[-1].faces
				else:
					os.remove(msg['outfile'])
				model_rating = db.update_model(model_id, 0)

def launch_worker(log=False):
	while 1:
		# Request a new assignment.
		comm.send(None, dest=MPI.ROOT, tag=ASSIGN)
		model_id = comm.recv(source=MPI.ROOT, tag=ASSIGN)

		# Run SevenAround and return surface.
		tmpname = 'tmp.%d.%s' % (rank, HDF5_EXT)
		surface = SevenAround(tmpname, model_id, log)
		comm.send(surface, dest=MPI.ROOT, tag=COMPLETE)

def exit_handler(signum, frame):
	print "rank %d caught SIGTERM or SIGINT..." % rank
	if rank == MPI.ROOT:
		for n in range(1, comm.Get_size()):
			print "worker %d finished %d assignments" % (n, assignments[n])
	sys.stdout.flush()
	comm.Barrier()
	comm.Abort(0)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-s', '--seed', action='store_true', help="seed mode: generate new low-level models instead of extending the highest rated model")
	parser.add_argument('-l', '--log', action='store_true', help="use a log-curved boundary")
	args = parser.parse_args()

	if comm.Get_size() < 2:
		raise ValueError("must use at least 2 MPI tasks")

	signal.signal(signal.SIGTERM, exit_handler)
	signal.signal(signal.SIGINT, exit_handler)

	if rank == MPI.ROOT:
		launch_controller(args.seed)
	else:
		launch_worker(args.log)

