// SevenAround - Constructing Models of the "Seven Around" Surface
// Copyright (c) 2009-2013 Mark Howison
// 
// This file is part of SevenAround.
// 
// SevenAround is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// SevenAround is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with SevenAround.  If not, see <http://www.gnu.org/licenses/>.

#include <cassert>
#include <cmath>
#include <fstream>
#include <hdf5.h>
#include <hdf5_hl.h>
#include "Surface.h"

/* creates levels 0 and 1 */
Surface::Surface(const bool _logBoundary)
{
	logBoundary = _logBoundary;

	Vertex* v[6];
	HalfEdge* e[3];
	Face* f[2];

	vcount = ecount = fcount = 0;

	vstack = NULL;
	estack = NULL;
	fstack = NULL;

	/* level 0 has one half-face at the origin */
	v[0] = newVertex(0, 0, 0, -1);
	v[0]->degree = 6;

	v[1] = newVertex(0.5, 0, 0, 0);
	v[1]->degree = 3;

	const double leg = HALF_SQRT3 * HALF_SQRT2;
	v[2] = newVertex(0, leg, leg, 0);
	v[2]->degree = 3;

	/* boundary edges aronud the half-face */
	e[0] = newHalfEdge(v[1]);
	e[1] = newHalfEdge(v[0]);
	e[1]->append(e[0]);

	f[0] = newFace(v[0], v[1], v[2], 0);

	/* link the boundary edges */
	e[1]->pair(f[0]->e1);
	e[0]->pair(f[0]->e3);

	/* level 1 adds a half-face on the x-axis,
	 * followed by four more faces that are fit
	 * in two pairs:
	 * the final of these faces lies along the y-axis */
	v[3] = newVertex(0.5 + leg, 0, -leg, -1);
	v[3]->degree = 5;

	/* boundary edge */
	xBoundary = newHalfEdge(v[3]);
	xBoundary->append(e[1]);

	v[4] = newVertex(0.5 + leg, 0.5, -leg, 1);

	f[1] = newFace(v[1], v[3], v[4], 1);
	f[1]->setCenter(0.5 + 0.5*leg, 0, -0.5*leg);

	/* link the boundary edge */
	xBoundary->pair(f[1]->e1);

	/* fit the first pair to complete the star at v[1] */
	bool success;
	success = fitEquilateralTriangles(
			f[1]->e3, f[0]->e2, -1, 1, FACE_BOUNDARY);
	assert(success);

	v[5] = newVertex(0, leg + HALF_SQRT2, leg + HALF_SQRT2, 1);
	v[5]->degree = 3;

	/* boundary edge + "dangling" edge */
	e[2] = newHalfEdge(v[2]);
	e[0]->append(e[2]);
	yBoundary = newHalfEdge(v[5]);
	e[2]->append(yBoundary);

	/* final pair connects the new edge on the y axis and completes
	 * the star of v[2] */
	success = fitEquilateralTriangles(
			e[2]->findTrailingEdge(), e[2], -1, 1, FACE_BOUNDARY);
	assert(success);

	/* e[2] was linked already by fitEquilateralTriangles */

	currentLevel = 1;

	xBoundaryType = 1;
	yBoundaryType = 1;

	randgen.seed();
}

Surface::Surface(const Surface* orig) { }

Surface::Surface(hid_t file, const char* model)
{
	const char* groupname = model ? model : "/";

	hid_t group = H5Gopen(file, groupname, H5P_DEFAULT);
	hsize_t n;

	H5LTget_attribute_int(group,".","currentLevel",&currentLevel);

	H5LTget_dataset_info(group,"V_id",&n,NULL,NULL);

	double* x = new double[n];
	double* y = new double[n];
	double* z = new double[n];
	int* leadedge = new int[n];
	int* level = new int[n];
	int* degree = new int[n];
	Vertex** V = new Vertex*[n];

	H5LTread_dataset_double(group,"V_x",x);
	H5LTread_dataset_double(group,"V_y",y);
	H5LTread_dataset_double(group,"V_z",z);
	H5LTread_dataset_int(group,"V_leadedge",leadedge);
	H5LTread_dataset_int(group,"V_level",level);
	H5LTread_dataset_int(group,"V_degree",degree);

	vcount = 0;
	vstack = NULL;
	// recreate objects in same order as previous run
	for (int i=0; i<n; i++) {
		V[i] = newVertex(x[i],y[i],z[i],level[i]);
		V[i]->degree = degree[i];
		if (degree[i] < 7 && level[i] >= 0) {
			incomplete[level[i]].push_back(V[i]);
		}
	}
	delete[] x, y, z, level, degree;

	H5LTget_dataset_info(group,"E_id",&n,NULL,NULL);

	int* origin = new int[n];
	int* next = new int[n];
	int* prev = new int[n];
	int* sibling = new int[n];
	HalfEdge** E = new HalfEdge*[n];

	H5LTread_dataset_int(group,"E_origin",origin);
	H5LTread_dataset_int(group,"E_next",next);
	H5LTread_dataset_int(group,"E_prev",prev);
	H5LTread_dataset_int(group,"E_sibling",sibling);

	ecount = 0;
	estack = NULL;
	// recreate objects in same order as previous run
	for (int i=0; i<n; i++) {
		E[i] = newHalfEdge(V[origin[i]]);
	}
	delete[] origin;

	int id;
	H5LTget_attribute_int(group,".","xBoundary",&id);
	H5LTget_attribute_int(group,".","xBoundaryType",&xBoundaryType);
	xBoundary = E[id];
	H5LTget_attribute_int(group,".","yBoundary",&id);
	H5LTget_attribute_int(group,".","yBoundaryType",&yBoundaryType);
	yBoundary = E[id];
	H5LTget_attribute_int(group,".","logBoundary",&id);
	logBoundary = (bool)id;

	for (int i=0; i<vcount; i++) {
		leadedge[i] >= 0 ? V[i]->leadedge = E[leadedge[i]] : V[i]->leadedge = NULL;
	}
	delete[] leadedge, V;

	for (int i=0; i<n; i++) {
		next[i] >= 0 ? E[i]->next = E[next[i]] : E[i]->next = NULL;
		prev[i] >= 0 ? E[i]->prev = E[prev[i]] : E[i]->prev = NULL;
		sibling[i] >= 0 ? E[i]->sibling = E[sibling[i]] : E[i]->sibling = NULL;
	}
	delete[] next, prev, sibling;

	H5LTget_dataset_info(group,"F_level",&n,NULL,NULL);

	x = new double[n];
	y = new double[n];
	z = new double[n];
	int* e1 = new int[n];
	int* e2 = new int[n];
	int* e3 = new int[n];
	level = new int[n];

	H5LTread_dataset_double(group,"F_x",x);
	H5LTread_dataset_double(group,"F_y",y);
	H5LTread_dataset_double(group,"F_z",z);
	H5LTread_dataset_int(group,"F_e1",e1);
	H5LTread_dataset_int(group,"F_e2",e2);
	H5LTread_dataset_int(group,"F_e3",e3);
	H5LTread_dataset_int(group,"F_level",level);

	fcount = (int)n;
	fstack = NULL;
	// recreate objects in same order as previous run
	for (int i=0; i<fcount; i++) {
		Face* f = new Face(E[e1[i]],E[e2[i]],E[e3[i]],level[i]);
		f->setCenter(x[i],y[i],z[i]);
		f->pop = fstack;
		fstack = f;
	}
	delete[] x, y, z, e1, e2, e3, level, E;

	H5Gclose(group);
	H5Fclose(file);

	randgen.seed();
}

Surface::~Surface()
{
	clear();
}

void Surface::clear()
{
	Vertex* v = vstack;
	while (v != NULL) {
		Vertex* vdel = v;
		v = v->pop;
		delete(vdel);
	}

	HalfEdge* e = estack;
	while (e != NULL) {
		HalfEdge* edel = e;
		e = e->pop;
		delete(edel);
	}

	Face* f = fstack;
	while (f != NULL) {
		Face* fdel = f;
		f = f->pop;
		delete(fdel);
	}
}

static double ipow_double(double x, int p)
{
	if (p == 0) return 1;
	if (p == 1) return x;

	double tmp = ipow_double(x, p/2);
	if (p%2 == 0) return tmp * tmp;
	else return x * tmp * tmp;
}

bool Surface::newLevel()
{
	Vertex*   v[3];
	HalfEdge* e;
	Face*	 f;
	double dx, dy, dz;
	bool success;

	currentLevel++;

	double angle;
	if (logBoundary) {
		//angle = 0.5 * PI * log((double)currentLevel) / (double)currentLevel;
		double x = (double)currentLevel;
		angle = atan(exp(currentLevel) - exp(currentLevel - 1.0));
	} else {
		angle = 0.25*PI;
	}

	const int direction = 2*(currentLevel % 2) - 1;

	// upper triangles along YZ-plane
	v[0] = yBoundary->origin;
	if (yBoundaryType == 1) {
		/* add a half-face to a single vertex */
		dy = HALF_SQRT3 * cos(angle);
		dz = HALF_SQRT3 * sin(angle);
		v[1] = newVertex(0.5, v[0]->y + dy, v[0]->z + dz, currentLevel);
		v[2] = newVertex(v[1], -1);
		v[2]->x = 0;
		v[2]->degree = 5;
		f = newFace(v[0], v[1], v[2], currentLevel);
		f->setCenter(0, v[0]->y + 0.5*dy, v[0]->z + 0.5*dz);
		if (!validateFace(FACE_BOUNDARY)) return false;
		/* link with boundary edge */
		yBoundary->pair(f->e3);
		/* boundary edge wraps around half-face */
		e = newHalfEdge(v[2]);
		e->pair(f->e2);
		yBoundary->append(e);
		yBoundary = e;
		/* fill in the gap using the trailing edge and the new face
		 * (direction alternates each level) */
		success = fitEquilateralTriangles(
			f->e1->findTrailingEdge(), f->e1, direction, currentLevel, FACE_BOUNDARY);
		if (!success) return false;
	} else if (yBoundaryType == 2) {
		/* add a half-face to an existing half-face edge */
		dy = HALF_SQRT3 * cos(angle);
		dz = HALF_SQRT3 * sin(angle);
		v[1] = newVertex(0, v[0]->y + dy, v[0]->z + dz, currentLevel);
		v[1]->degree = 3;
		f = newFace(yBoundary->sibling, v[1], currentLevel);
		f->setCenter(0, v[0]->y + 0.5*dy, v[0]->z + 0.5*dz);
		if (!validateFace(FACE_BOUNDARY)) return false;
		/* link with boundary edge */
		yBoundary->pair(f->e3);
		/* create new "dangling" boundary edge on the y axis */
		e = newHalfEdge(v[1]);
		yBoundary->append(e);
		yBoundary = e;
		/* ... and the next level's boundary face must be added now to avoid
		 * leaving v[1] with degree 6 at the start of the next level */
		v[0] = v[1];
		dy = 0.5 * cos(angle);
		dz = 0.5 * sin(angle);
		v[1] = newVertex(HALF_SQRT3, v[0]->y + dy, v[0]->z + dz, currentLevel+1);
		v[2] = newVertex(v[1], currentLevel+1);
		v[2]->x = 0;
		v[2]->y += dy;
		v[2]->z += dz;
		v[2]->degree = 3;
		f = newFace(v[0], v[1], v[2], currentLevel+1);
		if (!validateFace(FACE_BOUNDARY)) return false;
		/* link with boundary edge */
		yBoundary->pair(f->e3);
		/* create new "dangling" boundary edge on the y axis */
		e = newHalfEdge(v[2]);
		yBoundary->append(e);
		yBoundary = e;
		/* fill in the gap using the trailing edge and the new face
		 * (direction alternates each level) */
		success = fitEquilateralTriangles(
			f->e1->findTrailingEdge(), f->e1, direction, currentLevel, FACE_BOUNDARY);
		if (!success) return false;
		/* adjust level */
		fstack->level += 1;
	}
	yBoundaryType = (yBoundaryType + 1) % 3;

	// lower triangles along XZ-plane
	v[0] = xBoundary->origin;
	if (xBoundaryType == 0) {
		/* add a half-face to the existing boundary vertex */
		dx = HALF_SQRT3 * cos(angle);
		dz = HALF_SQRT3 * sin(angle);
		v[1] = newVertex(v[0]->x + dx, 0, v[0]->z - dz, -1);
		v[1]->degree = 6;
		v[2] = newVertex(v[1], currentLevel);
		v[2]->y = 0.5;
		f = newFace(v[0], v[1], v[2], currentLevel);
		f->setCenter(v[0]->x + 0.5*dx, 0, v[0]->z + 0.5*dz);
		if (!validateFace(FACE_BOUNDARY)) return false;
		/* create new boundary edge */
		e = newHalfEdge(v[1]);
		e->append(xBoundary);
		xBoundary = e;
		xBoundary->pair(f->e1);
		/* fill in the gap using the trailing edge and the new face
		 * (direction alternates each level) */
		e = v[0]->leadedge;
		success = fitEquilateralTriangles(
			e->findTrailingEdge(), e->findLeadingEdge(),
			direction, currentLevel, FACE_BOUNDARY);
		if (!success) return false;
	} else if (xBoundaryType == 1) {
		/* add an adjacent half-face to the existing half-face on
		 * the boundary */
		dx = HALF_SQRT3 * cos(angle);
		dz = HALF_SQRT3 * sin(angle);
		v[1] = newVertex(v[0]->x + dx, 0, v[0]->z - dz, currentLevel);
		v[1]->degree = 3;
		f = newFace(xBoundary->sibling->next, v[1], currentLevel);
		f->setCenter(v[0]->x + 0.5*dx, 0, v[0]->z + 0.5*dz);
		if (!validateFace(FACE_BOUNDARY)) return false;
		/* create new boundary edge */
		e = newHalfEdge(v[1]);
		e->append(xBoundary);
		xBoundary = e;
		xBoundary->pair(f->e2);
		/* ... and the next level's boundary face must be added now to avoid
		 * leaving v[1] with degree 6 at the start of the next level */
		v[0] = v[1];
		dx = cos(angle);
		dz = sin(angle);
		v[1] = newVertex(v[0]->x + dx, 0, v[0]->z - dz, currentLevel+1);
		v[1]->degree = 3;
		v[2] = newVertex(v[1], currentLevel+1);
		v[2]->x -= 0.5*dx;
		v[2]->y = HALF_SQRT3;
		v[2]->z += 0.5*dz;
		f = newFace(v[0], v[1], v[2], currentLevel+1);
		if (!validateFace(FACE_BOUNDARY)) return false;
		/* create new boundary edge */
		e = newHalfEdge(v[1]);
		e->append(xBoundary);
		xBoundary = e;
		xBoundary->pair(f->e1);
		/* fill in the gap using the trailing edge and the new face
		 * (direction alternates each level) */
		e = v[0]->leadedge;
		success = fitEquilateralTriangles(
			e->findTrailingEdge(), e->findLeadingEdge(),
			-direction, currentLevel, FACE_BOUNDARY);
		if (!success) return false;
		/* adjust level */
		fstack->pop->level += 1;
	}
	xBoundaryType = (xBoundaryType + 1) % 3;

	return true;
}

bool Surface::completeLevel(int maxAttempts)
{
	/* the vertices to complete are the ones behind
	 * the current face level */
	const int vLevel = currentLevel - 1;

	while (incomplete[vLevel].size() > 0)
	{
		Vertex* v = incomplete[vLevel].front();
		incomplete[vLevel].pop_front();

		assert(v->degree <= 7);
		assert(v->degree != 6);

		if (v->degree == 7) continue;

		int attempts = 0;
		int alternate = 0;

		while (v->degree < 5) {

			if (attempts == maxAttempts) {
#if DEBUG
				cout << "Exceeded max attempts!" << endl;
#endif
				return false;
			}

			alternate = (alternate + 1) % 2;

			//double theta = abs(randgen.randNorm(1.0,0.25*PI));
			double theta = randgen.randExc()*PI;
			if (alternate) theta *= -1;
			//HalfEdge trail = v->leadedge->findTrailingEdge();
			
			/* save the leadedge in case we have to revert */
			Face* f = newFace(v->leadedge->findLeadingEdge(), theta, currentLevel);
			if (!validateFace(FACE_NON_BOUNDARY)) {
				if (attempts > maxAttempts - 2) return false;
				deleteFace();
				deleteVertex();
				attempts++;
			}
		}
		
		assert(v->degree <= 7);
		assert(v->degree != 6);

		// Choose either 1 or -1 for the direction, up or down.
		int direction = 2 * randgen.randInt(1) - 1;
		// complete this star
		HalfEdge* leadedge = v->leadedge;
		bool success = fitEquilateralTriangles(
				v->leadedge->findTrailingEdge(),
				v->leadedge,
				direction,
				currentLevel,
				FACE_NON_BOUNDARY);
		if (!success) {
			assert(v->leadedge == leadedge);
			success = fitEquilateralTriangles(
					v->leadedge->findTrailingEdge(),
					v->leadedge,
					-direction,
					currentLevel,
					FACE_NON_BOUNDARY);
			if (!success) {
#if DEBUG
				cout << "Can't fit final two faces! (attempt " << attempts << ")" << endl;
#endif
				return false;
			}
		}
	}

#if DEBUG
	printf("Level %d is complete!\n", currentLevel);
#endif
	return true;
}

void Surface::makeModel(int id, int parent)
{
	this->id = id;
	this->parent = parent;
	attempts = 0;
	successes = 0;
	float = 0.f;
}

void Surface::updateRating(int success)
{
	successes += (success % 2);
	attempts += 1;
	rating = (float)successes / (float)attempts;
}

int Surface::size() { return fcount; }
float Surface::getRating() { return rating; }
int Surface::getID() { return id; }
void Surface::setParent(int parent) { this->parent = parent; }

/* private functions */

/* fits two equilateral triangles inbetween the edges E1 and E2
 * using one of the two possible directions */
bool Surface::fitEquilateralTriangles(
		HalfEdge* E1,
		HalfEdge* E2,
		int direction,
		int level,
		bool isBoundary)
{
	assert(E1 != NULL);
	assert(E2 != NULL);
	assert(E2->origin != NULL);
	assert(E1->next != NULL);
	assert(E1->next->origin != NULL);
	assert(E1->next->origin == E2->origin);
	assert(E2->next != NULL);
	assert(E2->next->origin != NULL);
	assert(direction == 1 || direction == -1);
	assert(E2->origin->degree == 5);

	Vector3 v1 = *(E1->origin);
	Vector3 v2 = *(E2->origin);
	Vector3 v3 = *(E2->next->origin);
	Vector3 diag = v3 - v1;
	/* the faces can only be inserted if the length of the diagonal from v1
	 * to v3 is <= 2 times the height of a unit equilateral triangle */
	double d = diag.squaredLength();
	if (d <= 3.0) {
		// the location of the new vertex that the two faces will share
		Vector3 v0;
		// m is the unit vector starting at v2 through the midpoint of the
		// diagonal
		Vector3 m = 0.5*diag + (v1 - v2);
		if (d < 3.0) {
			// m needs to be rotated in this case
			Quaternion rot;
			// Find the angle to rotate the midpoint vector away from
			// the E1 x E2 plane.
			double angle = direction * acos( 0.5 / m.length() );
			// The rotation will be about the diagonal, which therefore needs to
			// be normalized.
			diag.normalise();
			// The new vector should have length 1, so m needs to be
			// normalized too.
			m.normalise();
			rot.FromAngleAxis(angle,diag);
			v0 = v2 + rot * m;
		} else {
			v0 = v2 + m;
		}
		Vertex* v = newVertex(&v0, level);
		Face* f = newFace(E1, v, level);
		if (!validateFace(isBoundary)) {
			deleteFace();
			deleteVertex();
			return false;
		}
		newFace(f->e3, E2, level);
		if (!validateFace(isBoundary)) {
#if DEBUG
			cout << "Undoing fitEquilateralTriangle (deleting 2 faces)" << endl;
#endif
			deleteFace();
			deleteFace();
			deleteVertex();
			return false;
		}
		return true;
	}
	return false;
}

Vertex* Surface::newVertex(double x, double y, double z, int level)
{
	assert(level >= -1 && level < MAX_LEVEL);

	Vertex* v = new Vertex(x,y,z,level,0);
	assert(v != NULL);
	v->id = vcount++;

	// update the stack
	v->pop = vstack;
	vstack = v;

	if (level >= 0) incomplete[level].push_back(v);

	return v;
}

Vertex* Surface::newVertex(Vector3* vec, int level)
{
	return newVertex(vec->x,vec->y,vec->z,level);
}

Vertex* Surface::newVertex(Vector3 vec, int level)
{
	return newVertex(vec.x,vec.y,vec.z,level);
}

HalfEdge* Surface::newHalfEdge(Vertex* origin)
{
	assert(origin != NULL);

	/* create the edge */
	HalfEdge *e = new HalfEdge(origin);
	assert(e != NULL);
	e->id = ecount++;

	/* epdate the edge stack */
	e->pop = estack;
	estack = e;

	return e;
}

Face* Surface::newFace(Vertex* V1, Vertex* V2, Vertex* V3, int level)
{
	assert(V1 != NULL && V2 != NULL && V3 != NULL);
	assert(level >= 0 && level < MAX_LEVEL);

	HalfEdge *e1 = newHalfEdge(V1);
	HalfEdge *e2 = newHalfEdge(V2);
	HalfEdge *e3 = newHalfEdge(V3);

	/* link edges */
	e1->append(e2);
	e2->append(e3);
	e3->append(e1);

	/* increment vertex degree */
	V1->degree++;
	V2->degree++;
	V3->degree++;

	/* create the face */
	Face *f = new Face(e1, e2, e3, level);
	assert(f != NULL);

	if (V1->leadedge == NULL) V1->leadedge = e1;
	if (V2->leadedge == NULL) V2->leadedge = e2;
	if (V3->leadedge == NULL) V3->leadedge = e3;

	/* update the face stack */
	f->pop = fstack;
	fstack = f;
	fcount++;

	return f;
}

Face* Surface::newFace(HalfEdge* E, Vertex* V, int level)
{
	assert(E != NULL && V != NULL);
	assert(E->origin != V);
	assert(E->next->origin != V);

	Vertex* V0 = E->next->origin;
	Face* f = newFace(V0, E->origin, V, level);

	/* pair siblings */
	E->pair(f->e1);

	/* fix leading edges */
	E->origin->leadedge = E->findLeadingEdge();
	V0->leadedge = E->next->findLeadingEdge();

	return f;
}

Face* Surface::newFace(HalfEdge* E1, HalfEdge* E2, int level)
{
	assert(E1 != NULL && E2 != NULL && E2->next != NULL);
	assert(E1->next->origin == E2->origin);

	Vertex* v1 = E2->origin;
	Vertex* v2 = E1->origin;
	Vertex* v3 = E2->next->origin;

	assert(v1->degree == 6);

	Face* f = newFace(v1, v2, v3, level);

	/* pair siblings */
	E1->pair(f->e1);
	E2->pair(f->e3);

	/* cancel leading edge */
	v1->leadedge = NULL;
	
	return f;
}

Face* Surface::newFace(HalfEdge* E, double angle, int level)
{
	assert(E != NULL);
	assert(E->next != NULL);
	assert(E->next != E);
	assert(E->next->origin != NULL);
	assert(E->next->origin != E->origin);
	assert(E->prev != NULL);
	assert(E->prev != E);
	assert(E->prev->origin != NULL);
	assert(E->prev->origin != E->origin);

	Vector3 origin, axis, altitude;
	Quaternion rot;
	Vertex* v;

	origin = *(E->origin);
	/* the axis of rotation is the input edge E */
	axis = *(E->next->origin) - origin;
	assert((axis.length() - 1.0) < EPSILON);
	//axis.normalise();
	//assert(axis.length() == 1.0);
	/* move the origin to the midpoint of E */
	origin += 0.5 * axis;
	/* the altitude of the face that E belongs to */
	altitude = *(E->prev->origin) - origin;
	/* create the rotation */
	rot = Quaternion(PI + angle, axis);
	/* the new vertex is the rotated altitude */
	v = newVertex(origin + rot * altitude, level);

	return newFace(E,v,level);
}

void Surface::deleteVertex()
{
	Vertex* v = vstack;
	vstack = vstack->pop;
	vcount--;
	if (v->level >= 0) {
		assert(incomplete[v->level].back() == v);
		incomplete[v->level].pop_back();
	}
	delete(v);
}

void Surface::deleteFace()
{
	/* pop face */
	Face* f = fstack;
	fstack = f->pop;
	fcount--;

	/* pop edges */
	assert(f->e3 == estack);
	estack = estack->pop;
	assert(f->e2 == estack);
	estack = estack->pop;
	assert(f->e1 == estack);
	estack = estack->pop;
	ecount -= 3;

	/* cache sibling pairings then nullify them and update leading edges */
	HalfEdge* e[3];
	e[0] = f->e1->sibling;
	e[1] = f->e2->sibling;
	e[2] = f->e3->sibling;
	if (e[0] != NULL) {
		assert(e[0]->sibling == f->e1);
		assert(e[0] != f->e1);
		e[0]->sibling = NULL;
		e[0]->origin->leadedge = e[0]->findLeadingEdge();
	}
	if (e[1] != NULL) {
		assert(e[1]->sibling == f->e2);
		assert(e[1] != f->e2);
		e[1]->sibling = NULL;
		e[1]->origin->leadedge = e[1]->findLeadingEdge();
	}
	if (e[2] != NULL) {
		assert(e[2]->sibling == f->e3);
		assert(e[2] != f->e3);
		e[2]->sibling = NULL;
		e[2]->origin->leadedge = e[2]->findLeadingEdge();
	}

	/* decrement vertex degree */
	f->e1->origin->degree--;
	f->e2->origin->degree--;
	f->e3->origin->degree--;

	delete(f->e1);
	delete(f->e2);
	delete(f->e3);
	delete(f);
}

double Surface::distSq(Vertex* V1, Vertex* V2) const
{
	Vector3 vec = *V1 - *V2;
	return vec.squaredLength();
}

double Surface::deg2rad(double rad) const
{
	return rad * PI / 180.0;
}

// borrowed from Jonathon Shewchuk's exact arithmetic geometry predicates
// (this one is inexact and fast)
double Surface::orient3dfast(
	double *pa,
	double *pb,
	double *pc,
	double *pd) const
{
	double adx, bdx, cdx;
	double ady, bdy, cdy;
	double adz, bdz, cdz;

	adx = pa[0] - pd[0];
	bdx = pb[0] - pd[0];
	cdx = pc[0] - pd[0];
	ady = pa[1] - pd[1];
	bdy = pb[1] - pd[1];
	cdy = pc[1] - pd[1];
	adz = pa[2] - pd[2];
	bdz = pb[2] - pd[2];
	cdz = pc[2] - pd[2];

	return adx * (bdy * cdz - bdz * cdy)
		+ bdx * (cdy * adz - cdz * ady)
		+ cdx * (ady * bdz - adz * bdy);
}

bool Surface::detectCollision(Face* f1, Face* f2) const
{
	// local vars
	double* a1;
	double* a2;
	double* a3;
	double* b1;
	double* b2;
	double* b3;
	double o[3];
	double oo[3];
	// first test if face1 intersects the plane of face0
	a1 = &(f1->e1->origin->x);
	a2 = &(f1->e2->origin->x);
	a3 = &(f1->e3->origin->x);
	b1 = &(f2->e1->origin->x);
	b2 = &(f2->e2->origin->x);
	b3 = &(f2->e3->origin->x);
	o[0] = orient3dfast(a1,a2,a3,b1);
	o[1] = orient3dfast(a1,a2,a3,b2);
	o[2] = orient3dfast(a1,a2,a3,b3);
	oo[0] = o[0]*o[1];
	oo[1] = o[1]*o[2];
	oo[2] = o[2]*o[0];
	if (oo[0] > 0.0 && oo[1] > 0.0) return false;
	else {
		// test each edge of face1 for intersection with face0
		if (oo[0] <= 0.0) {
			o[0] = orient3dfast(a1,a2,b1,b2);
			o[1] = orient3dfast(a2,a3,b1,b2);
			o[2] = orient3dfast(a3,a1,b1,b2);
			if (o[0]*o[1] > 0.0 && o[0]*o[2] > 0.0) return true;
		}
		if (oo[1] <= 0.0) {
			o[0] = orient3dfast(a1,a2,b2,b3);
			o[1] = orient3dfast(a2,a3,b2,b3);
			o[2] = orient3dfast(a3,a1,b2,b3);
			if (o[0]*o[1] > 0.0 && o[0]*o[2] > 0.0) return true;
		}
		if (oo[2] <= 0.0) {
			o[0] = orient3dfast(a1,a2,b3,b1);
			o[1] = orient3dfast(a2,a3,b3,b1);
			o[2] = orient3dfast(a3,a1,b3,b1);
			if (o[0]*o[1] > 0.0 && o[0]*o[2] > 0.0) return true;
		}
		// now reverse the roles of face1 and face0
		o[0] = orient3dfast(b1,b2,b3,a1);
		o[1] = orient3dfast(b1,b2,b3,a2);
		o[2] = orient3dfast(b1,b2,b3,a3);
		oo[0] = o[0]*o[1];
		oo[1] = o[1]*o[2];
		oo[2] = o[2]*o[0];
		if (oo[0] > 0.0 && oo[1] > 0.0) return false;
		else {
			// test each edge of face0 for intersection with face1
			if (oo[0] <= 0.0) {
				o[0] = orient3dfast(b1,b2,a1,a2);
				o[1] = orient3dfast(b2,b3,a1,a2);
				o[2] = orient3dfast(b3,b1,a1,a2);
				if (o[0]*o[1] > 0.0 && o[0]*o[2] > 0.0) return true;
			}
			if (oo[1] <= 0.0) {
				o[0] = orient3dfast(b1,b2,a2,a3);
				o[1] = orient3dfast(b2,b3,a2,a3);
				o[2] = orient3dfast(b3,b1,a2,a3);
				if (o[0]*o[1] > 0.0 && o[0]*o[2] > 0.0) return true;
			}
			if (oo[2] <= 0.0) {
				o[0] = orient3dfast(b1,b2,a3,a1);
				o[1] = orient3dfast(b2,b3,a3,a1);
				o[2] = orient3dfast(b3,b1,a3,a1);
				if (o[0]*o[1] > 0.0 && o[0]*o[2] > 0.0) return true;
			}
		}
	}
	return false;
}


bool Surface::validateFace(bool isBoundary) const
{
	Face* f = fstack;

	/* enforce 4-fold symmetry */
	if (!isBoundary) {
		if (f->e1->origin->x <= 0) return false;
		if (f->e1->origin->y <= 0) return false;
		if (f->e2->origin->x <= 0) return false;
		if (f->e2->origin->y <= 0) return false;
		if (f->e3->origin->x <= 0) return false;
		if (f->e3->origin->y <= 0) return false;
	}

	/* check for collisions with faces that are within
	 * the spherical hull of the new face */
	Face* f1 = f->pop;
	while (f1 != NULL) {
		if (f->squaredDistance(*f1) <= FOUR_THIRDS) {
			if (!f->isAdjacent(f1)) {
				if (detectCollision(f,f1)) return false;
			}
	}
	f1 = f1->pop;
	}

	return true;
}

void Surface::writeOOGL(ostream& out) const
{
	int i, j;
	string colors[5];
	// the colors for the rings
	colors[0] = "0.85 0.0 0.0 1.0"; // red
	colors[1] = "1.0 0.42 0.0 1.0"; // orange
	colors[2] = "1.0 0.94 0.0 1.0"; // yellow
	colors[3] = "0.22 1.0 0.0 1.0"; // green
	colors[4] = "0.28 0.0 1.0 1.0"; // blue
	out << "OFF" << endl;
	out.setf(ios::fixed);
	out << 4*vcount << " " << 4*fcount << " 0" << endl << endl;
	Vertex* v = vstack;
	while (v != NULL) {
		out <<  v->x << " " <<  v->y << " " << v->z << endl;
		out << -v->x << " " <<  v->y << " " << v->z << endl;
		out <<  v->x << " " << -v->y << " " << v->z << endl;
		out << -v->x << " " << -v->y << " " << v->z << endl;
		out << endl;
		v = v->pop;
	}
	// write the faces
	Face* f = fstack;
	while (f != NULL) {
		for (j=0; j<4; j++) {
			// invert the ids because we walked the stack backwards
			// when outputing the vertices
			out << "3 "
				 << 4*(vcount - f->e1->origin->id - 1)+j << " "
				 << 4*(vcount - f->e2->origin->id - 1)+j << " "
				 << 4*(vcount - f->e3->origin->id - 1)+j << " "
				 << colors[(f->level)%5] << endl;
		}
		out << endl;
		f = f->pop;
	}
}

void Surface::writeHDF5(hid_t file) const
{
	hsize_t count;

	H5LTset_attribute_int(file,"/","currentLevel",&currentLevel,1);

	double* x = new double[vcount];
	double* y = new double[vcount];
	double* z = new double[vcount];
	int* leadedge = new int[vcount];
	int* level = new int[vcount];
	int* degree = new int[vcount];
	int* id = new int[vcount];

	Vertex* v = vstack;
	for (int i=vcount-1; i>=0; i--) {
		x[i] = v->x;
		y[i] = v->y;
		z[i] = v->z;
		v->leadedge != NULL ? leadedge[i] = v->leadedge->id : leadedge[i] = -1;
		level[i] = v->level;
		degree[i] = v->degree;
		id[i] = v->id;
		v = v->pop;
	}

	count = vcount;
	H5LTmake_dataset(file,"V_x",1,&count,H5T_NATIVE_DOUBLE,x);
	H5LTmake_dataset(file,"V_y",1,&count,H5T_NATIVE_DOUBLE,y);
	H5LTmake_dataset(file,"V_z",1,&count,H5T_NATIVE_DOUBLE,z);
	H5LTmake_dataset(file,"V_leadedge",1,&count,H5T_NATIVE_INT,leadedge);
	H5LTmake_dataset(file,"V_level",1,&count,H5T_NATIVE_INT,level);
	H5LTmake_dataset(file,"V_degree",1,&count,H5T_NATIVE_INT,degree);
	H5LTmake_dataset(file,"V_id",1,&count,H5T_NATIVE_INT,id);

	delete[] x, y, z, leadedge, level, degree, id;

	int* origin = new int[ecount];
	int* next = new int[ecount];
	int* prev = new int[ecount];
	int* sibling = new int[ecount];
	id = new int[ecount];

	HalfEdge* e = estack;
	for (int i=ecount-1; i>=0; i--) {
		origin[i] = e->origin->id;
		e->next ? next[i] = e->next->id : next[i] = -1;
		e->prev ? prev[i] = e->prev->id : prev[i] = -1;
		e->sibling ? sibling[i] = e->sibling->id : sibling[i] = -1;
		id[i] = e->id;
		e = e->pop;
	}

	count = ecount;
	H5LTmake_dataset(file,"E_origin",1,&count,H5T_NATIVE_INT,origin);
	H5LTmake_dataset(file,"E_next",1,&count,H5T_NATIVE_INT,next);
	H5LTmake_dataset(file,"E_prev",1,&count,H5T_NATIVE_INT,prev);
	H5LTmake_dataset(file,"E_sibling",1,&count,H5T_NATIVE_INT,sibling);
	H5LTmake_dataset(file,"E_id",1,&count,H5T_NATIVE_INT,id);

	int i = xBoundary->id;
#if DEBUG
	printf("xBoundary = %d\n",i);
#endif
	H5LTset_attribute_int(file,"/","xBoundary",&i,1);
	H5LTset_attribute_int(file,"/","xBoundaryType",&xBoundaryType,1);

	i = yBoundary->id;
#if DEBUG
	printf("yBoundary = %d\n",i);
#endif
	H5LTset_attribute_int(file,"/","yBoundary",&i,1);
	H5LTset_attribute_int(file,"/","yBoundaryType",&yBoundaryType,1);

	i = (int)logBoundary;
	H5LTset_attribute_int(file,"/","logBoundary",&i,1);

	delete[] origin, next, prev, sibling, id;

	x = new double[fcount];
	y = new double[fcount];
	z = new double[fcount];
	int* e1 = new int[fcount];
	int* e2 = new int[fcount];
	int* e3 = new int[fcount];
	level = new int[fcount];

	Face* f = fstack;
	for (int i=fcount-1; i>=0; i--) {
		x[i] = f->x;
		y[i] = f->y;
		z[i] = f->z;
		e1[i] = f->e1->id,
		e2[i] = f->e2->id,
		e3[i] = f->e3->id,
		level[i] = f->level;
		f = f->pop;
	}

	count = fcount;
	H5LTmake_dataset(file,"F_x",1,&count,H5T_NATIVE_DOUBLE,x);
	H5LTmake_dataset(file,"F_y",1,&count,H5T_NATIVE_DOUBLE,y);
	H5LTmake_dataset(file,"F_z",1,&count,H5T_NATIVE_DOUBLE,z);
	H5LTmake_dataset(file,"F_e1",1,&count,H5T_NATIVE_INT,e1);
	H5LTmake_dataset(file,"F_e2",1,&count,H5T_NATIVE_INT,e2);
	H5LTmake_dataset(file,"F_e3",1,&count,H5T_NATIVE_INT,e3);
	H5LTmake_dataset(file,"F_level",1,&count,H5T_NATIVE_INT,level);

	delete[] x, y, z, e1, e2, e3, level;

	H5Fclose(file);
}

void Surface::dump() const
{
	printf("\nVERTICES\n");
	printf("--------\n");
	printf("	ID |		x |		y |		z | level | deg | leadedge\n");
	Vertex* v = vstack;
	while (v != NULL) {
		int lead;
		v->leadedge != NULL ? lead = v->leadedge->id : lead = -1;
		printf (" %5d | %+8.3f | %+8.3f | %+8.3f |	%2d |   %1d |	%5d\n",
				v->id, v->x, v->y, v->z, v->level, v->degree, lead);
		v = v->pop;
	}
	printf("\nEDGES\n");
	printf("-----\n");
	printf("	ID |  from ->	to |  next |  prev |   sib\n");
	HalfEdge* e = estack;
	while (e != NULL) {
		int to, next, prev, sibling;
		e->next ? to = e->next->origin->id : to = -1;
		e->next ? next = e->next->id : next = -1;
		e->prev ? prev = e->prev->id : prev = -1;
		e->sibling ? sibling = e->sibling->id : sibling = -1;
		printf (" %5d | %5d -> %5d | %5d | %5d | %5d\n",
				e->id, e->origin->id, to, next, prev, sibling);
		e = e->pop;
	}
	printf("xBoundary: %d (type %d), yBoundary: %d (type %d)\n",
			xBoundary->id, xBoundaryType, yBoundary->id, yBoundaryType);
	printf("\nFACES\n");
	printf("-----\n");
	printf("	ID |	V1 |	V2 |	V3 | level \n");
	Face* f = fstack;
	int i = 1;
	while (f != NULL) {
		printf (" %5d | %5d | %5d | %5d |	%2d\n",
				fcount - i++,
				f->e1->origin->id,
				f->e2->origin->id,
				f->e3->origin->id,
				f->level);
		f = f->pop;
	}
}

void Surface::dumpIncomplete()
{
	printf("\nINCOMPLETE\n");
	printf("----------\n");
	printf(" queue |	ID | level | deg\n");
	for (int i=0; i<MAX_LEVEL; i++) {
		for (int j=0; j < incomplete[i].size(); j++) {
			Vertex* v = incomplete[i][j];
			printf ("	%2d | %5d |	%2d |   %1d\n",
					i, v->id, v->level, v->degree);;
		}
	}
	printf("\n");
	cout << "Faces = " << fcount << endl;
	cout << "CurrentLevel = " << currentLevel << endl;
}

